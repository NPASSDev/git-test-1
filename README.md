# Git Submodules

#### Why?

Use submodules to include dependencies of other project into another project (that is a git repo into another git repo) and working with both the repos seemlessly as if it was a single code base.

Git submodules help us in ease of coding in multiple repos while also maintaing separate repos.

#### Example Repos 

* [R1](https://bitbucket.org/NPASSDev/git-test-1/src/master/) - git@bitbucket.org:NPASSDev/git-test-1.git
* [R2](https://bitbucket.org/NPASSDev/git-test-2/src/master/) - git@bitbucket.org:NPASSDev/git-test-2.git
* [R3](https://bitbucket.org/NPASSDev/git-test-3/src/master/) - git@bitbucket.org:NPASSDev/git-test-3.git


#### How to setup?

1. Assume there are 3 repos. R1, R2, R3.
2. Assume that R2, R3 are helper / util repos which are to be used alongside multiple other application code repos like R1.
3. Clone R1 on local.
4. Add R2 and R3 as submodules to R1. This creates 2 directories in R1 as submodules and a `.gitmodules` file.
```
git submodule add <git-address-of-R1>
git submodule add <git-address-of-R2>
```
Commit `.gitmodules` file along with the directories. Note that git stores the latest commit in the submodules in the parent repo.
Do `git ls-tree master` to check the objects of type commit which correspond to submodules.

#### Using once first time setup is done

1. Clone R1 on local
2. Run command `git submodule init`. This copies info from `.gitmodules` committed file from repo to local `.git/config` file.
3. Run `git submodule update --recursive --remote --merge` to get latest changes from central repos of submodule.


#### Making changes to submodules

1. cd to submodule and do the changes
2. Each submodule is a valid git repo and all the git commands are valid.
3. Once changes are done add and commit the changes within the submodule using `git add` and `git commit`.
4. Push the changes to central repo of submodule using `git push`.
5. Go to the parent module. On running `git status` it will show that there are new commits in submodule. It will not show the exact changes since that is the job of submodule. Parent repo just tracks that there are some new commits.
6. Run `git add` and `git commit` to add these new submodule changes to parent repo. Note that parent repo doesnt save the actual changes but just the latest commit.

_** Step 4 is very important as commits of submodule need to be pushed to central repo of submodule else another developer who checks out the parent repo and tries to run it will face issues since the parent repo will be pointing to a commit id of submodule which is not in the central repo of submodule and hence not available to other developers **_


#### Yet to explore

* Tools for PR review of parent PR and submodule PR. Need to explore - 
* Make scripts using `git submodule foreach` so that devs dont have to run commands to update submodules everytime.
* Defining a workflow around submodules with forks and branches. Also defining a workflow around PR reviews.

#### References

* https://www.atlassian.com/git/tutorials/git-submodule
* https://stackoverflow.com/questions/5033441/where-does-git-store-the-sha1-of-the-commit-for-a-submodule
* https://stackoverflow.com/questions/5828324/update-git-submodule-to-latest-commit-on-origin


